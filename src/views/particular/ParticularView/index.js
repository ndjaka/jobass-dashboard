import React, { useCallback, useEffect, useState } from 'react';
import { Box, Container, makeStyles } from '@material-ui/core';

import Results from './components/Results';
import Toolbar from './components/Toolbar';

import { useDispatch, useSelector } from 'react-redux';

import { useSnackbar } from 'notistack';

import Page from '../../../components/Page';
import { getListParticularEffect } from '../../../store/effects/ParticularEffects';


const useStyles = makeStyles(theme => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3)
  }
}));

const JobListView = () => {
  const classes = useStyles();
  const { data } = useSelector(state => state.particular);
  const [loading, setLoading] = useState(false);
  const dispatch = useDispatch();
  const { enqueueSnackbar } = useSnackbar();
  const [page, setPage] = useState(0);
  const [limit, setLimit] = useState(10);
  const [keyword, setKeyword] = useState('');

  const URL_BY_PAGE = `particular?limit=${limit}&page=${page}`;
  

  const fetchData = () => {
    dispatch(getListParticularEffect(URL_BY_PAGE, setLoading, enqueueSnackbar));
  };

  useEffect(() => {
    fetchData();
  }, [page, limit]);

/*   useEffect(() => {
    if (keyword !== '') {
      dispatch(getListJobsEffect(URL_BY_KEYWORD, setLoading, enqueueSnackbar));
    } else {
      dispatch(getListJobsEffect(URL_BY_PAGE, setLoading, enqueueSnackbar));
    }
  }, [keyword]); */

  return (
    <Page className={classes.root} title="Particulier">
      <Container maxWidth={false}>
        <Toolbar loading={loading} keyword={keyword} setKeyword={setKeyword}  fetchData={fetchData} />
        <Box mt={3}>
          <Results
            limit={limit}
            setLimit={setLimit}
            page={page}
            setPage={setPage}
            particulars={data && data.particulars}
            length={data && data.length}
            fetchData={fetchData}
          />
        </Box>
      </Container>
    </Page>
  );
};

export default JobListView;
