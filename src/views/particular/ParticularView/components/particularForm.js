import React, { useState } from 'react';
import FormBuilder from '../../../../components/FormBuilder';
import * as Yup from 'yup';
import { Button, Typography, Grid, Box, makeStyles } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import {
  AddJobEffect,
  getEmployerEffect
} from '../../../../store/effects/JobEffects';
import { useSnackbar } from 'notistack';

const useStyles = makeStyles(theme => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    height: '100%',
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3)
  }
}));

export default function ParticularForm({ onCloseForm, type, data }) {
  const classes = useStyles();
  const dispatch = useDispatch();
  const session = useSelector(state => state.session);
  const [loading, setLoading] = useState(true);
  const { enqueueSnackbar } = useSnackbar();
  const [employers, setEmployers] = useState([]);

  React.useEffect(() => {
    getEmployerEffect(setEmployers, enqueueSnackbar);
  }, []);
  const forms = [
    {
      type: 'file_drop_zone',
      label: 'Profil',
      name: 'profilePic',
      required: true,
      containerProps: {
        lg: 6,
        md: 6,
        sm: 12,
        xs: 12
      }
    },{
      type: 'file_drop_zone',
      label: 'Carte d\'identité',
      name: 'identityCard',
      required: true,
      containerProps: {
        lg: 6,
        md: 6,
        sm: 12,
        xs: 12
      }
    },  {
      type: 'text',
      label: 'Nom',
      name: 'name',
      required: true,
      containerProps: {
        lg: 6,
        md: 6,
        sm: 12,
        xs: 12
      }
    },
    {
      type: 'text',
      label: 'Prénom',
      name: 'surname',
      required: true,
      containerProps: {
        lg: 6,
        md: 6,
        sm: 12,
        xs: 12
      }
    },
    {
      type: 'text',
      label: 'Email',
      name: 'email',
      required: true,
      containerProps: {
        lg: 6,
        md: 6,
        sm: 12,
        xs: 12
      }
    },
    {
      type: 'text',
      label: 'Numéro de téléphone',
      name: 'phoneNumber',
      required: true,
      containerProps: {
        lg: 6,
        md: 6,
        sm: 12,
        xs: 12
      }
    },
    {
      type: 'password',
      label: 'Mot de passe',
      name: 'password',
      required: true,
      containerProps: {
        lg: 6,
        md: 6,
        sm: 12,
        xs: 12
      }
    },

    {
      type: 'date',
      label: 'Date de naissance',
      name: 'birthday',
      required: true,
      row: 4,
      multiline: true,
      containerProps: {
        lg: 6,
        md: 12,
        sm: 12,
        xs: 12
      }
    },
    {
      type: 'list-check-boxes',
      label: 'Type de compte',
      name: 'state',
      datas: ['employee', 'employer'],
      containerProps: {
        lg: 6,
        md: 12,
        sm: 12,
        xs: 12
      }
    },
    {
      type: 'radio',
      label: 'Genre',
      name: 'gender',
      required: true,
      radioGroup: [
        {
          value: 'Man',
          label: 'Homme'
        },
        {
          value: 'Woman',
          label: 'Femme'
        }
      ],
      containerProps: {
        lg: 6,
        md: 12,
        sm: 12,
        xs: 12
      }
    },
    {
      type: 'select',
      label: 'Comment avez vous découvert Jobaas ?',
      name: 'origin',
      selectOptions: [
        {
          value: 'Friends',
          label: 'Friends'
        },
        {
          value: 'Instagram',
          label: 'Instagram'
        },
        {
          value: 'Facebook',
          label: 'Facebook'
        },
        {
          value: 'Whatsapp',
          label: 'Whatsapp'
        },
        {
          value: 'LinkedIn',
          label: 'LinkedIn'
        },
        {
          value: 'Youtube',
          label: 'Youtube '
        }
      ],
      containerProps: {
        lg: 6,
        md: 6,
        sm: 12,
        xs: 12
      }
    },
    {
      type: 'text',
      label: 'Profession',
      name: 'profession',

      containerProps: {
        lg: 6,
        md: 6,
        sm: 12,
        xs: 12
      }
    },
    {
      type: 'text',
      label: 'Compte a utilisé pour les transactions',
      name: 'MoneyAccount',
      containerProps: {
        lg: 6,
        md: 6,
        sm: 12,
        xs: 12
      }
    },

    {
      type: 'chip-input',
      label: 'Connaissances',
      name: 'skills',
      containerProps: {
        lg: 6,
        md: 6,
        sm: 12,
        xs: 12
      }
    },
    {
      type: 'chip-input',
      label: 'Logiciel',
      name: 'software',
      containerProps: {
        lg: 6,
        md: 6,
        sm: 12,
        xs: 12
      }
    },

    {
      type: 'text',
      label: 'Description',
      multiline: true,
      row: 5,
      name: 'description',
      containerProps: {
        lg: 12,
        md: 6,
        sm: 12,
        xs: 12
      }
    },
    {
      type: 'text',
      label: 'Ville',
      name: 'town',
      containerProps: {
        lg: 4,
        md: 6,
        sm: 12,
        xs: 12
      }
    },
    {
      type: 'text',
      label: 'Rue',
      name: 'street',
      containerProps: {
        lg: 4,
        md: 6,
        sm: 12,
        xs: 12
      }
    },

    {
      type: 'text',
      label: 'Rue de reference',
      name: 'referenceStreet',
      containerProps: {
        lg: 4,
        md: 6,
        sm: 12,
        xs: 12
      }
    },

    {
      type: 'select',
      label: 'Vehicule',
      name: 'vehicle',
      selectOptions: [
        {
          value: 'car',
          label: 'Car'
        },
        {
          value: 'bike',
          label: 'Moto'
        },
        {
          value: 'bus',
          label: 'Bus'
        }
      ],
      containerProps: {
        lg: 4,
        md: 6,
        sm: 12,
        xs: 12
      }
    },
    {
      type: 'select',
      label: 'Catégorie',
      name: 'category',
      selectOptions: [
        {
          value: 'A',
          label: 'A'
        },
        {
          value: 'B',
          label: 'B'
        },
        {
          value: 'C',
          label: 'C'
        },
        {
          value: 'D',
          label: 'D'
        },
        {
          value: 'E',
          label: 'E'
        },
        {
          value: 'AM',
          label: 'AM'
        },
        {
          value: 'A1',
          label: 'A1'
        },
        {
          value: 'A2',
          label: 'A2'
        },
        {
          value: 'B1',
          label: 'B1'
        },
        {
          value: 'BE',
          label: 'BE'
        },
        {
          value: 'C1E',
          label: 'C1E'
        },
        {
          value: 'CE',
          label: 'CE'
        },
        {
          value: 'D1',
          label: 'D1'
        },
        {
          value: 'D1E',
          label: 'D1E'
        }
      ],
      containerProps: {
        lg: 4,
        md: 6,
        sm: 12,
        xs: 12
      }
    },
    {
      type: 'date',
      label: 'Date de délivrance du permis',
      name: 'date',
      containerProps: {
        lg: 4,
        md: 6,
        sm: 12,
        xs: 12
      }
    },
    {
      type: 'list-check-boxes',
      label: 'Tags ',
      name: 'tags',

      datas: [
        'Art',
        'Education',
        'House',
        'Delivery',
        'Events',
        'Commercial',
        'Distribution',
        'Transport',
        'Technical_support',
        'Animals',
        'Beauty_bodycare',
        'Sport',
        'Health',
        'Fashion',
        'Web_service',
        'Others',
        'Network_multimedia',
        'Relocation',
        'Office_service',
        'Hotel_catering'
      ],
      containerProps: {
        lg: 12,
        md: 12,
        sm: 12,
        xs: 12
      }
    },

    {
      type: 'text',
      label: 'Niveau',
      name: 'level',
      containerProps: {
        lg: 6,
        md: 12,
        sm: 12,
        xs: 12
      }
    },
    {
      type: 'text',
      label: "Année d'obtention du diplome",
      name: 'diplomeYear',
      containerProps: {
        lg: 6,
        md: 12,
        sm: 12,
        xs: 12
      }
    },
    {
      type: 'language',
      label: 'Langues',
      name: 'language',
      containerProps: {
        lg: 12,
        md: 12,
        sm: 12,
        xs: 12
      }
    }
  ];

  const validations = {
    name: Yup.string()
      .max(255)
      .required('La nom  est requise'),
    surname: Yup.string()
      .max(255)
      .required('La prenom  est requis'),
    email: Yup.string()
      .max(255)
      .email('email invalide')
      .required('Le titre est requis'),
    phoneNumber: Yup.string()
      .max(255)
      .required('Le numéro de téléphone est requis'),
    password: Yup.string()
      .max(255)
      .required('Le mot de passe est requis'),
    birthday: Yup.string()
      .max(255)
      .required('La date de naissance est requise'),
    gender: Yup.string().required('Le genre est requis')
  };

  const initialValues = {
    name: '',
    surname: '',
    email: '',
    phoneNumber: '',
    password: '',
    birthday: '',
    state: [],
    gender: 'Man',
    origin: '',
    profession: '',
    MoneyAccount: '',
    skills: [],
    software: [],
    description: '',
    town: '',
    street: '',
    referenceStreet: '',
    vehicle: '',
    category: '',
    date: '',
    tags: [],
    level: '',
    diplomeYear: '',
    language: ''
  };

  return (
    <div>
      <FormBuilder
        Field={forms}
        initialValues={initialValues}
        validationSchema={validations}
        renderSubmit={(
          isSubmitting,
          errors,
          touched,
          setFieldTouched,
          handleSubmit
        ) => (
          <Grid item xs={12}>
            <Box mt={2}>
              <Button
                color="primary"
                // disabled={isSubmitting}

                type="submit"
                  onClick={() => {

                    handleSubmit();
                    console.log('errors', errors);
                  }}
                variant="contained"
              >
                se connecter
              </Button>
            </Box>
          </Grid>
        )}
        onSubmit={async (values, { setErrors, setStatus, setSubmitting }) => {
        /*   const data = {
            initial: values.firstname[0] + values.lastname[0],
            origin: values.origin,
            profession: values.profession,
            software: values.software,
            language: values.language,
            driver_permit: {
              vehicle: values.car,
              date: values.date,
              category: values.category
            },
            skills: values.skills,
            name: values.name,
            surname: values.surname,
            description: values.description,
            MoneyAccount: [values.MoneyAccount],
            phoneNumber: {
              value: values.phoneNumber
            },
            gender: values.gender,
            email: {
              value: values.value
            },
            /* "valid": false, */
            /*  "registrationDate": "2020-11-29T09:51:13.373Z", */
          //   schoolLevel: {
          //     level: values.level,
          //     diplomaYear: values.diplomaYear
          //     /* "valid": false */
          //   },
          //   profilePic: {
          //     url:
          //       'https://static.actu.fr/uploads/2020/05/presentation-avatars-fb.jpg'
          //     /*  "valid": false */
          //   },
          //   identityCard: {
          //     url:
          //       'https://static.actu.fr/uploads/2020/05/presentation-avatars-fb.jpg'
          //     /*  "valid": false */
          //   },
          //   password: values.password,
          //   town: values.town,
          //   street: values.street,
          //   tags: values.tags,
          //   referenceStreet: values.referenceStreet,
          //   state: values.state,
          //   birthday: values.birthday
          // }; 

          console.log('values', values);
          /*  dispatch(
            AddJobEffect(data, setLoading, enqueueSnackbar, onCloseForm)
          ); */
        }}
      />
    </div>
  );
}
