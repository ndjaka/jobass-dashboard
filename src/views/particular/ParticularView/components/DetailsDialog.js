import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import Typography from '@material-ui/core/Typography';
import CloseIcon from '@material-ui/icons/Close';
import {
  Box,
  Container,
  DialogContent,
  DialogTitle,
  Grid,
  Table,
  TableCell,
  TableRow
} from '@material-ui/core';
import moment from 'moment';
export default function DetailsDialog({data, open, onClose }) {
  const handleClose = () => {
    onClose();
  };
  console.log('data', data);
/*   const datas = {
    job: {
      registrationDate: '2020-11-17T20:01:41.713Z',
      isValid: 'false',
      town: 'string',
      street: 'string',
      referenceStreet: 'string',
      title: 'string',
      description: 'string',
      state: 'created',
      employer: 'string',
      nbplaces: 1,
      startDate: '2020-11-17T20:01:41.713Z',
      endDate: '2020-11-17T20:01:41.713Z',
      price: 0,
      employerPayment: 1000,
      frequency: {
        value_frequency: {
          frequencies: 'monday',
          timeSlots: [
            {
              startHour: 'string',
              endHour: 'string'
            }
          ]
        },
        chooseCandidate: true,
        isRegular: true,
        listOfDates: [
          {
            day: '2020-11-17',
            timeSlots: [
              {
                startHour: 'string',
                endHour: 'string'
              }
            ]
          }
        ]
      },
      prerequisites: ['string'],
      tags: ['string'],
      nbViews: 1,
      typeEmployer: 'created'
    },
    infos: 'string'
  }; */

  return (
    <div>
      <Dialog  open={open} onClose={handleClose} maxWidth={'lg'} fullWidth>

        <DialogTitle id="alert-dialog-title">
          informations sur le jobs
        </DialogTitle>
        <DialogContent>
          <Grid container spacing={4}>
            <Grid item lg={6} md={6} sm={12} xs={12}>
              <Table>
                <TableRow>
                  <TableCell>
                    <Typography variant="h6">Vile</Typography>
                  </TableCell>
                  <TableCell align={'center'}>{data && data.job && data.job.town}</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>
                    <Typography variant="h6">Rue</Typography>
                  </TableCell>
                  <TableCell align={'center'}>{data && data.job && data.job.street}</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>
                    <Typography variant="h6">Rue de refence</Typography>
                  </TableCell>
                  <TableCell align={'center'}>
                    {data && data.job && data.job.referenceStreet}
                  </TableCell>
                </TableRow>
                
                <TableRow>
                  <TableCell>
                    <Typography variant="h6">Nombre de place</Typography>
                  </TableCell>
                  <TableCell align={'center'}>{data && data.job && data.job.nbplaces}</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>
                    <Typography variant="h6">Etat</Typography>
                  </TableCell>
                  <TableCell align={'center'}>{data && data.job && data.job.state}</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>
                    <Typography variant="h6">Date de debut</Typography>
                  </TableCell>
                  <TableCell align={'center'}>
                    {moment(data && data.job && data.job.startDate).format('DD/MM/YYYY')}
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>
                    <Typography variant="h6">Date de fin</Typography>
                  </TableCell>
                  <TableCell align={'center'}>
                    {moment(data && data.job && data.job.endDate).format('DD/MM/YYYY')}
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>
                    <Typography variant="h6">Prix</Typography>
                  </TableCell>
                  <TableCell align={'center'}>{data && data.job && data.job.price}</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>
                    <Typography variant="h6">Listes des dates</Typography>
                  </TableCell>
                  <TableCell>
                    {data && data.job && data.job.frequency.listOfDates.map(item => (
                      <Box mt={1}>
                        <Box component="span" display="flex">
                          <Typography variant="h6">jour:</Typography>
                          {item.day}
                        </Box>

                        {item.timeSlots.map(it => (
                          <>
                            <Box component="span" display="flex">
                              <Typography variant="h6">
                                Date de debut :
                              </Typography>
                              {it.startHour}
                            </Box>

                            <Box component="span" display="flex">
                              <Typography variant="h6">Date de fin:</Typography>
                              {it.endHour}
                            </Box>
                          </>
                        ))}
                      </Box>
                    ))}
                  </TableCell>
                </TableRow>
              </Table>
            </Grid>

            <Grid item lg={6} md={6} sm={12} xs={12}>
              <Table>
                <TableRow>
                  <TableCell>
                    <Typography variant="h6">Employé payer</Typography>
                  </TableCell>
                  <TableCell align={'center'}>
                    {data && data.job && data.job.employerPayment}
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>
                    <Typography variant="h6">Jour et creneaux</Typography>
                  </TableCell>
                  <TableCell>
                    <Box display="flex">
                      <Typography variant="h6">Jour:</Typography>
                      {data && data.job && data.job.frequency.value_frequency.frequencies}
                    </Box>

                    {data && data.job && data.job.frequency.value_frequency.timeSlots && data.job && data.job.frequency.value_frequency.timeSlots.map(item => (
                      <Box mt={1}>
                        <Typography variant="h6">créneau:</Typography>

                        <Box component="span" display="flex">
                          <Typography variant="h6">Date de debut :</Typography>
                          {item.startHour}
                        </Box>

                        <Box component="span" display="flex">
                          <Typography variant="h6">Date de fin:</Typography>
                          {item.endHour}
                        </Box>
                      </Box>
                    ))}
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>
                    <Typography variant="h6">candidat choisis</Typography>
                  </TableCell>
                  <TableCell align={'center'}>
                    {data && data.job && data.job.frequency.chooseCandidate}
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>
                    <Typography variant="h6">Regulier?</Typography>
                  </TableCell>
                  <TableCell align={'center'}>
                    {data && data.job && data.job.frequency.isRegular}
                  </TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>
                    <Typography variant="h6">prérequis</Typography>
                  </TableCell>
                  <TableCell align={'center'}>{data && data.job && data.job.prerequisites && data.job && data.job.prerequisites.map(item=>item).join(',')}</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>
                    <Typography variant="h6">Etiquette</Typography>
                  </TableCell>
                  <TableCell align={'center'}>{open &&  open.job && open.job.tags.map(item=>item).join(',')}</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>
                    <Typography variant="h6">Nombre de vue</Typography>
                  </TableCell>
                  <TableCell align={'center'}>{data && data.job && data.job.nbViews}</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>
                    <Typography variant="h6">Infos</Typography>
                  </TableCell>
                  <TableCell align={'center'}>{data && data.infos && data.infos.name}</TableCell>
                </TableRow>
              </Table>
            </Grid>
            <Grid item lg={12} md={12} sm={12} xs={12}>
            <TableRow>
                  <TableCell>
                    <Typography variant="h6">Description</Typography>
                  </TableCell>
                  <TableCell align={'center'}>
                    {data && data.job && data.job.description}
                  </TableCell>
                </TableRow>
            </Grid>
          </Grid>
        </DialogContent>
      </Dialog>
    </div>
  );
}
