import React, { useState } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import {
  Box,
  Button,
  Card,
  CardContent,
  TextField,
  InputAdornment,
  SvgIcon,
  makeStyles,
  Typography,
  CircularProgress
} from '@material-ui/core';
import { Search as SearchIcon } from 'react-feather';
import AddDialog from './AddDialog';

const useStyles = makeStyles(theme => ({
  root: {},
  importButton: {
    marginRight: theme.spacing(1)
  },
  exportButton: {
    marginRight: theme.spacing(1)
  },
  label: {
    textTransform: 'uppercase'
  }
}));

const Toolbar = ({fetchData, className, loading, setKeyword, keyword, ...rest }) => {
  const classes = useStyles();
  const [open, setOpen] = useState(false);
  return (
    <div className={clsx(classes.root, className)} {...rest}>
      {
        open && <AddDialog fetchData={fetchData} open={open} onClose={()=>setOpen(false)}/>
      }
      <Box display="flex" justifyContent="space-between">
        <Box className={classes.label}>
          <Typography variant={'h3'}>Particuliers</Typography>
        </Box>
        <Button color="primary" variant="contained" onClick={()=>setOpen(true)}>
          Ajouter un particulier
        </Button>
      </Box>
      <Box mt={3}>
        <Card>
          <CardContent>
            <Box maxWidth={500}>
              <TextField
                value={keyword}
                onChange={e => setKeyword(e.target.value)}
                fullWidth
                InputProps={
                  ({
                    startAdornment: (
                      <InputAdornment position="start">
                        <SvgIcon fontSize="small" color="action">
                          <SearchIcon />
                        </SvgIcon>
                      </InputAdornment>
                    )
                  },
                  {
                    endAdornment: (
                      <InputAdornment position="end">
                        {loading && <CircularProgress size={24} />}
                      </InputAdornment>
                    )
                  })
                }
                placeholder="Search particuliers"
                variant="outlined"
              />
            </Box>
          </CardContent>
        </Card>
      </Box>
    </div>
  );
};

Toolbar.propTypes = {
  className: PropTypes.string
};

export default Toolbar;
