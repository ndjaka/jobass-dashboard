import React, { useState } from 'react';
import FormBuilder from '../../../../components/FormBuilder';
import * as Yup from 'yup';
import { Button, Typography, Grid, Box, makeStyles } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import {
  AddJobEffect,
  getEmployerEffect,
  updateJobEffect
} from '../../../../store/effects/JobEffects';
import { useSnackbar } from 'notistack';

const useStyles = makeStyles(theme => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    height: '100%',
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3)
  }
}));

export default function JobForm2({ onCloseForm, type, data }) {
  const classes = useStyles();
  const dispatch = useDispatch();
  const session = useSelector(state => state.session);
  const [loading, setLoading] = useState(true);
  const { enqueueSnackbar } = useSnackbar();
  const [employers, setEmployers] = useState([]);
  const [display, setDisplay] = useState(
    data.job && data.job.frequency.isRegular ? 'regular' : 'ponctual'
  );
  let listOfDates = [];

  React.useEffect(() => {
    getEmployerEffect(setEmployers, enqueueSnackbar);
  }, []);

  data.job &&
    data.job.frequency.listOfDates &&
    data.job.frequency.listOfDates.forEach((element, index) => {
      listOfDates.push({
        day: element.day,
        startHour: element.timeSlots && element.timeSlots[0] && element.timeSlots[0].startHour,
        endHour: element.timeSlots && element.timeSlots[0] && element.timeSlots[0].endHour
      });
    });

  console.log('data', data);

  const forms = [
    {
      type: 'list-check-boxes',
      label: 'Catégories ',
      name: 'tags',
      required: true,
      datas: [
        'Art',
        'Education',
        'House',
        'Delivery',
        'Events',
        'Commercial',
        'Distribution',
        'Transport',
        'Technical_support',
        'Animals',
        'Beauty_bodycare',
        'Sport',
        'Health',
        'Fashion',
        'Web_service',
        'Others',
        'Network_multimedia',
        'Relocation',
        'Office_service',
        'Hotel_catering'
      ],
      containerProps: {
        lg: 12,
        md: 12,
        sm: 12,
        xs: 12
      }
    },
    {
      type: 'text',
      label: 'Titre de la Mission',
      name: 'title',
      required: true,
      containerProps: {
        lg: 6,
        md: 6,
        sm: 12,
        xs: 12
      }
    },
    {
      type: 'text',
      label: 'Description',
      name: 'description',
      required: true,
      row: 4,
      multiline: true,
      containerProps: {
        lg: 12,
        md: 12,
        sm: 12,
        xs: 12
      }
    },
    {
      type: 'text',
      label: 'Ville',
      name: 'town',
      required: true,
      containerProps: {
        lg: 6,
        md: 6,
        sm: 12,
        xs: 12
      }
    },
    {
      type: 'text',
      label: 'Rue',
      name: 'street',
      required: true,
      containerProps: {
        lg: 6,
        md: 6,
        sm: 12,
        xs: 12
      }
    },
    {
      type: 'text',
      label: 'Rue de reference',
      name: 'referenceStreet',
      containerProps: {
        lg: 6,
        md: 6,
        sm: 12,
        xs: 12
      }
    },

    {
      type: 'chip-input',
      label:
        'Préréquis (appuiyer sur la touche entrer pour ajouter un préréquis)',
      name: 'prerequisites',
      containerProps: {
        lg: 12,
        md: 12,
        sm: 12,
        xs: 12
      }
    },
    {
      type: 'text',
      label: 'Slug',
      name: 'slug',
      containerProps: {
        lg: 6,
        md: 6,
        sm: 12,
        xs: 12
      }
    },
    {
      type: 'text',
      label: 'Raison',
      name: 'reason',
      containerProps: {
        lg: 6,
        md: 6,
        sm: 12,
        xs: 12
      }
    },

    {
      type: 'radio',
      label: 'Etat du jobs',
      name: 'state',
      radioGroup: [
        {
          value: 'in progress',
          label: 'En progression'
        },
        {
          value: 'completed',
          label: 'Complet'
        },
        {
          value: 'created',
          label: 'Crée'
        },
        {
          value: 'paid',
          label: 'Payer'
        },
        {
          value: 'deleted ',
          label: 'Supprimé'
        },
        {
          value: 'validé ',
          label: 'validated'
        }
      ],
      containerProps: {
        lg: 12,
        md: 12,
        sm: 12,
        xs: 12
      }
    },
    {
      type: 'text',
      label: 'Le nombre de personnes qui doivent consulter cet emploi',
      name: 'nbViews',
      containerProps: {
        lg: 6,
        md: 6,
        sm: 12,
        xs: 12
      }
    },
    {
      type: 'radio',
      label: 'Le job sera validé ?',
      name: 'isValid',
      radioGroup: [
        {
          value: 'Oui',
          label: 'Oui'
        },
        {
          value: 'Non',
          label: 'Non'
        }
      ],
      containerProps: {
        lg: 6,
        md: 6,
        sm: 12,
        xs: 12
      }
    },
    {
      type: 'text',
      label: 'Nombre de place',
      name: 'nbplaces',
      containerProps: {
        lg: 6,
        md: 6,
        sm: 12,
        xs: 12
      }
    },
    {
      type: 'text',
      label: 'Nombre de place restantes',
      name: 'nbPlacesLeft',
      containerProps: {
        lg: 6,
        md: 6,
        sm: 12,
        xs: 12
      }
    },
    {
      type: 'text',
      label: 'Prix',
      name: 'price',
      containerProps: {
        lg: 6,
        md: 6,
        sm: 12,
        xs: 12
      }
    },
    {
      type: 'text',
      label: "Prix à payer par l'employer hors mis les taxes",
      name: 'employerPayment',
      required: true,
      containerProps: {
        lg: 6,
        md: 6,
        sm: 12,
        xs: 12
      }
    },

    {
      type: 'auto-complete',
      label: 'Employé ',
      name: 'employer',

      required: true,
      datas: employers,
      containerProps: {
        lg: 6,
        md: 6,
        sm: 12,
        xs: 12
      }
    },
    {
      type: 'radio',
      label: "Type d'employé ?",
      name: 'typeEmployer',
      changeState: setDisplay,
      radioGroup: [
        {
          value: 'particular',
          label: 'Particulier'
        },
        {
          value: 'company',
          label: 'Compagnie'
        }
      ],
      containerProps: {
        lg: 6,
        md: 6,
        sm: 12,
        xs: 12
      }
    },

    {
      type: 'radio',
      label: 'Mode de sélection des candidatures',
      name: 'chooseCandidate',
      required: true,
      radioGroup: [
        {
          value: true,
          label: 'Je laisse Jobass se charger des candidatures'
        },
        {
          value: false,
          label: 'Je préfère sélectionner mes candidats'
        }
      ],
      containerProps: {
        lg: 12,
        md: 12,
        sm: 12,
        xs: 12
      }
    },
    {
      type: 'radio',
      label: 'Fréquence du travail',
      name: 'isRegular',
      changeState: setDisplay,
      radioGroup: [
        {
          value: 'regular',
          label: 'Régulière'
        },
        {
          value: 'ponctual',
          label: 'Ponctuelle'
        }
      ],
      containerProps: {
        lg: 4,
        md: 12,
        sm: 12,
        xs: 12
      }
    },
    {
      type: 'date',
      label: 'Date de debut',
      name: 'startDate',

      required: true,
      containerProps: {
        lg: 4,
        md: 12,
        sm: 12,
        xs: 12
      }
    },
    {
      type: 'date',
      label: 'Date de fin',
      name: 'endDate',

      required: true,
      containerProps: {
        lg: 4,
        md: 12,
        sm: 12,
        xs: 12
      }
    },
    {
      type: 'select',
      label: 'Jour',
      name: 'frequencies',
      show: display === 'regular' ? false : true,
      selectOptions: [
        {
          value: 'monday',
          label: 'Lundi'
        },
        {
          value: 'tuesday',
          label: 'Mardi'
        },
        {
          value: 'wednesday',
          label: 'Mercredi'
        },
        {
          value: 'thursday',
          label: 'jeudi'
        },
        {
          value: 'friday',
          label: 'Vendredi'
        },
        {
          value: 'saturday',
          label: 'Samedi'
        },
        {
          value: 'sunday ',
          label: 'Dimanche'
        }
      ],
      containerProps: {
        lg: 4,
        md: 12,
        sm: 12,
        xs: 12
      }
    },
    {
      type: 'custom-input',
      label: 'Créneau horaire',
      name: 'timeSlots_hour',
      show: display === 'regular' ? false : true,
      containerProps: {
        lg: 8,
        md: 12,
        sm: 12,
        xs: 12
      }
    },
    {
      type: 'ponctual-date',
      label: 'Créneau horaire',
      show: display === 'ponctual' ? false : true,
      name: 'ponctual_date',
      containerProps: {
        lg: 12,
        md: 12,
        sm: 12,
        xs: 12
      }
    }
  ];

  const validations = {
    town: Yup.string()
      .max(255)
      .required('La ville  est requise'),
    street: Yup.string()
      .max(255)
      .required('La rue  est requise'),
    title: Yup.string()
      .max(255)
      .required('Le titre est requis'),
    description: Yup.string().required('Le description est requise'),
    employer: Yup.string()
      .max(255)
      .required("L'employé est requis"),
    startDate: Yup.string()
      .max(255)
      .required('La date de debut est requise'),
    endDate: Yup.string()
      .max(255)
      .required('La date de fin est requise'),
    employerPayment: Yup.string()
      .max(255)
      .required('Ce champ est requis'),
    tags: Yup.string().required('Ce champ est requis')
  };

  const initialValues = {
    title: type === 'update' ? data && data.job && data.job.title : '',
    town: type === 'update' ? data.job && data.job.town : '',
    street: type === 'update' ? data.job && data.job.street : '',
    slug: type === 'update' ? data.job && data.job.slug : '',
    reason: type === 'update' ? data.job && data.job.reason : '',
    referenceStreet:
      type === 'update' ? data.job && data.job.referenceStreet : '',
    description: type === 'update' ? data.job && data.job.description : '',
    state: type === 'update' ? data.job && data.job.state : '',
    isValid:
      type === 'update'
        ? data.job && data.job.isValid
          ? 'Oui'
          : 'Non'
        : 'Non',
    nbplaces: type === 'update' ? data.job && data.job.nbplaces : '',
    nbViews: type === 'update' ? data.job && data.job.nbViews : '',
    nbPlacesLeft: type === 'update' ? data.job && data.job.nbPlacesLeft : '',
    price: type === 'update' ? data.job && data.job.price : '',
    startDate: type === 'update' ? data.job && data.job.startDate : '',
    endDate: type === 'update' ? data.job && data.job.endDate : '',
    employer:
      type === 'update'
        ? employers.find(emp => emp.id === data.job.employer)
        : '',
    typeEmployer: type === 'update' ? data.job && data.job.typeEmployer : '',
    employerPayment:
      type === 'update' ? data.job && data.job.employerPayment : '',
    tags: type === 'update' ? data.job && data.job.tags : [],
    prerequisites: type === 'update' ? data.job && data.job.prerequisites : [],
    chooseCandidate:
      type === 'update' ? data.job && data.job.chooseCandidate : true,
    frequencies:
      type === 'update'
        ? data.job &&
          data.job.frequency.value_frequency[0] &&
          data.job.frequency.value_frequency[0].day
        : true,
    timeSlots_hour:
      type === 'update'
        ? data.job &&
          data.job.frequency.value_frequency[0] &&
          data.job.frequency.value_frequency[0].timeSlots.map(item => ({
            startHour: item.startHour,
            endHour: item.endHour
          }))
        : [],
    isRegular:
      type === 'update'
        ? data.job && data.job.frequency.isRegular
          ? 'regular'
          : 'ponctual'
        : 'regular',
    ponctual_date: type === 'update' ? listOfDates : []
  };

  console.log(
    'list',
    data.job &&
      data.job.frequency.listOfDates &&
      data.job.frequency.listOfDates.map((item, index) => ({
        day: item.day,
        startHour: item.timeSlots && item.timeSlots[0] && item.timeSlots[0].startHour,
        endHour: item.timeSlots && item.timeSlots[0] && item.timeSlots[0].endHour
      }))
  );

  return (
    <div>
      <FormBuilder
        Field={forms}
        initialValues={initialValues}
        validationSchema={validations}
        renderSubmit={(
          isSubmitting,
          errors,
          touched,
          setFieldTouched,
          handleSubmit
        ) => (
          <Grid item xs={12}>
            <Box mt={2}>
              <Button
                color="primary"
                // disabled={isSubmitting}
                size="large"
                type="submit"
                onClick={handleSubmit}
                variant="contained"
                >
                  {type !== 'update' ? '  enregister':'Modifier'}
              
              </Button>
            </Box>
          </Grid>
        )}
        onSubmit={async (values, { setErrors, setStatus, setSubmitting }) => {
          const _data = {
            town: values.town,
            street: values.street,
            referenceStreet: values.referenceStreet,
            title: values.title,
            description: values.description,
            employer: values.employer.id,
            state: values.state,
            isValid: values.isValid === 'Oui' ? true : false,
            nbplaces: values.nbplaces,
            nbPlacesLeft: values.nbPlacesLeft,
            startDate: values.startDate,
            endDate: values.endDate,
            chooseCandidate: values.chooseCandidate === 'Oui' ? true : false,
            frequency: {
              isRegular: values.isRegular === 'regular' ? true : false,
              value_frequency:
                values.isRegular === 'ponctual'
                  ? []
                  : {
                      day: values.frequencies,
                      timeSlots: values.timeSlots_hour
                    },
              listOfDates:
                values.isRegular === 'regular' ? [] : values.ponctual_date
            },
            employerPayment: values.employerPayment,
            price: values.price,
            prerequisites: values.prerequisites,
            slug: values.slug,
            tags: values.tags,
            nbViews: values.nbViews,
            typeEmployer: values.typeEmployer,
            reason: values.reason
          };

          {
            type !== 'update'
              ? dispatch(
                  AddJobEffect(_data, setLoading, enqueueSnackbar, onCloseForm)
                )
              : dispatch(
                  updateJobEffect(
                    _data,
                    data && data.job && data.job._id,
                    setLoading,
                    enqueueSnackbar,
                    onCloseForm
                  )
                );
          }
        }}
      />
    </div>
  );
}
