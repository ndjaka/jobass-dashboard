import React, { useState } from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import moment from 'moment';
import PerfectScrollbar from 'react-perfect-scrollbar';
import {
  Avatar,
  Box,
  Card,
  Checkbox,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  Typography,
  makeStyles,
  IconButton
} from '@material-ui/core';
import VisibilityIcon from '@material-ui/icons/Visibility';
import DetailsDialog from './DetailsDialog';
import { getJobEffect } from '../../../../store/effects/JobEffects';
import { useSnackbar } from 'notistack';
import EditIcon from '@material-ui/icons/Edit';
import AddDialog from './AddDialog';
const useStyles = makeStyles(theme => ({
  root: {},
  avatar: {
    marginRight: theme.spacing(2)
  }
}));

const Results = ({
  className,
  limit,
  setLimit,
  page,
  setPage,
  customers,
  jobs,
  length,
  fetchData,
  ...rest
}) => {
  const classes = useStyles();
  const [openDetails, setOpenDetails] = useState(false);
  const [data, setData] = useState({});
  const [updateDialog, setUpdateDialog] = useState(null);
  const { enqueueSnackbar } = useSnackbar();

  const handleLimitChange = event => {
    setLimit(event.target.value);
  };

  const handlePageChange = (event, newPage) => {
    setPage(newPage);
  };

  const fectJob = id => {
    getJobEffect(id, setData, enqueueSnackbar);
  };

const fectJobForUpdate = id => {
    getJobEffect(id, setUpdateDialog, enqueueSnackbar);
  };

  return (
    <Card className={clsx(classes.root, className)} {...rest}>
      {openDetails && (
        <DetailsDialog
          data={data}
          open={openDetails}
          onClose={() => setOpenDetails(false)}
        />
      )}
      {!!updateDialog && (
        <AddDialog
          fetchData={fetchData}
          type={'update'}
          open={updateDialog}
          onClose={() => setUpdateDialog(null)}
        />
      )}

      <PerfectScrollbar>
        <Box minWidth={1050}>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>Employer type</TableCell>
                <TableCell>Employer Payment</TableCell>
                <TableCell>Location</TableCell>
                <TableCell>Places left</TableCell>
                <TableCell>Registration date</TableCell>
                <TableCell>actions</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {jobs &&
                jobs.slice(0, limit).map(job => (
                  <TableRow
                    hover
                    key={job._id}
                    /* selected={selectedCustomerIds.indexOf(customer.id) !== -1} */
                  >
                    {/*  <TableCell padding="checkbox">
                    <Checkbox
                      checked={selectedCustomerIds.indexOf(customer.id) !== -1}
                      onChange={(event) => handleSelectOne(event, customer.id)}
                      value="true"
                    />
                  </TableCell> */}
                    <TableCell>
                      <Box alignItems="center" display="flex">
                        {/*  <Avatar
                        className={classes.avatar}
                        src={customer.avatarUrl}
                      >
                        {getInitials(customer.name)}
                      </Avatar> */}
                        <Typography color="textPrimary" variant="body1">
                          {job.typeEmployer}
                        </Typography>
                      </Box>
                    </TableCell>
                    <TableCell>{job.employerPayment}</TableCell>
                    <TableCell>
                      {`${job.town}, ${job.street}, ${job.referenceStreet}`}
                    </TableCell>
                    <TableCell>{job.nbPlacesLeft}</TableCell>
                    <TableCell>
                      {moment(job.registrationDate).format('DD/MM/YYYY')}
                    </TableCell>
                    <TableCell>
                      <IconButton
                        onClick={() => {
                          fectJob(job._id);
                          setOpenDetails(true);
                        }}
                      >
                        <VisibilityIcon />
                      </IconButton>
                    </TableCell>
                    <TableCell>
                      <IconButton onClick={() => fectJobForUpdate(job._id)}>
                        <EditIcon />
                      </IconButton>
                    </TableCell>
                  </TableRow>
                ))}
            </TableBody>
          </Table>
        </Box>
      </PerfectScrollbar>
      <TablePagination
        component="div"
        count={length}
        onChangePage={handlePageChange}
        onChangeRowsPerPage={handleLimitChange}
        page={page}
        rowsPerPage={limit}
        rowsPerPageOptions={[5, 15, 25, 35]}
      />
    </Card>
  );
};

Results.propTypes = {
  className: PropTypes.string,
  customers: PropTypes.array.isRequired,
  jobs: PropTypes.array.isRequired
};

export default Results;
