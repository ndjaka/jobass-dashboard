import {
  Container,
  Button,
  Grid,
  Link,
  TextField,
  Typography,
  makeStyles,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  Box,
  RadioGroup,
  FormControlLabel,
  Radio,
  Checkbox,
  FormHelperText
} from '@material-ui/core';
import { Formik } from 'formik';
import ChipInput from 'material-ui-chip-input';
import { useSnackbar } from 'notistack';
import React, { useState } from 'react';
import * as Yup from 'yup';
import TimeSlots from '../../../../components/TimeSlots';
import {
  AddJobEffect,
  getEmployerEffect
} from '../../../../store/effects/JobEffects';
import { useDispatch, useSelector } from 'react-redux';
import { Autocomplete } from '@material-ui/lab';
import DateField from '../../../../components/DateField';

const useStyles = makeStyles(theme => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    height: '100%',
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3)
  }
}));

export default function JobForm() {
  const classes = useStyles();
  const dispatch = useDispatch();
  const session = useSelector(state => state.session);
  const [loading, setLoading] = useState(true);
  const { enqueueSnackbar } = useSnackbar();
  const [employers, setEmployers] = useState([]);

  React.useEffect(() => {
    getEmployerEffect(setEmployers, enqueueSnackbar);
  }, []);

  const forms = [
    {
      type: 'text',
      label: 'Titre',
      name: 'title',
      required: true,
      containerProps: {
        lg: 6,
        md: 6,
        sm: 12,
        xs: 12
      }
    },
    {
      type: 'text',
      label: 'Ville',
      name: 'town',
      required: true,
      containerProps: {
        lg: 6,
        md: 6,
        sm: 12,
        xs: 12
      }
    },
    {
      type: 'text',
      label: 'Rue',
      name: 'street',
      required: true,
      containerProps: {
        lg: 6,
        md: 6,
        sm: 12,
        xs: 12
      }
    },
    {
      type: 'text',
      label: 'Rue de reference',
      name: 'referenceStreet',
      containerProps: {
        lg: 6,
        md: 6,
        sm: 12,
        xs: 12
      }
    },
    {
      type: 'text',
      label: 'Slug',
      name: 'slug',
      containerProps: {
        lg: 6,
        md: 6,
        sm: 12,
        xs: 12
      }
    },
    {
      type: 'text',
      label: 'Raison',
      name: 'reason',
      containerProps: {
        lg: 6,
        md: 6,
        sm: 12,
        xs: 12
      }
    },
    {
      type: 'text',
      label: 'Description',
      name: 'description',
      required: true,
      row: 4,
      multiline: true,
      containerProps: {
        lg: 12,
        md: 12,
        sm: 12,
        xs: 12
      }
    },
    {
      type: 'radio',
      label: 'Etat du jobs',
      name: 'state',
      radioGroup: [
        {
          value: 'in progress',
          label: 'En progression'
        },
        {
          value: 'completed',
          label: 'Complet'
        },
        {
          value: 'created',
          label: 'Crée'
        },
        {
          value: 'paid',
          label: 'Payer'
        },
        {
          value: 'deleted ',
          label: 'Supprimé'
        }
      ],
      containerProps: {
        lg: 12,
        md: 12,
        sm: 12,
        xs: 12
      }
    },
    {
      type: 'text',
      label: 'Le nombre de personnes qui doivent consulter cet emploi',
      name: 'nbViews',
      containerProps: {
        lg: 6,
        md: 6,
        sm: 12,
        xs: 12
      }
    },
    {
      type: 'radio',
      label: 'Le job sera validé ?',
      name: 'isValid',
      radioGroup: [
        {
          value: 'Oui',
          label: 'Oui'
        },
        {
          value: 'Non',
          label: 'Non'
        }
      ],
      containerProps: {
        lg: 6,
        md: 6,
        sm: 12,
        xs: 12
      }
    },
    {
      type: 'text',
      label: 'Nombre de place',
      name: 'nbplaces',
      containerProps: {
        lg: 6,
        md: 6,
        sm: 12,
        xs: 12
      }
    },
    {
      type: 'text',
      label: 'Nombre de place restantes',
      name: 'nbPlacesLeft',
      containerProps: {
        lg: 6,
        md: 6,
        sm: 12,
        xs: 12
      }
    },
    {
      type: 'text',
      label: 'Prix',
      name: 'price',
      containerProps: {
        lg: 6,
        md: 6,
        sm: 12,
        xs: 12
      }
    },
    {
      type: 'text',
      label: "Prix à payer par l'employer hors mis les taxes",
      name: 'employerPayment',
      required: true,
      containerProps: {
        lg: 6,
        md: 6,
        sm: 12,
        xs: 12
      }
    },
   
    {
      type: 'date',
      label: 'Date de debut',
      name: 'startDate',

      required: true,
      containerProps: {
        lg: 6,
        md: 6,
        sm: 12,
        xs: 12
      }
    },
    {
      type: 'date',
      label: 'Date de fin',
      name: 'endDate',

      required: true,
      containerProps: {
        lg: 6,
        md: 6,
        sm: 12,
        xs: 12
      }
    },

    
    {
      type: 'auto-complete',
      label: 'Employé ',
      name: 'employer',

      required: true,
      datas: employers,
      containerProps: {
        lg: 6,
        md: 6,
        sm: 12,
        xs: 12
      }
    },
    {
      type: 'radio',
      label: "Type d'employé ?",
      name: 'typeEmployer',
      radioGroup: [
        {
          value: 'particular',
          label: 'Particulier'
        },
        {
          value: 'company',
          label: 'Compagnie'
        }
      ],
      containerProps: {
        lg: 6,
        md: 6,
        sm: 12,
        xs: 12
      }
    },

    {
      type: 'list-check-boxes',
      label: 'Etiquette ',
      name: 'tags',
      required: true,
      datas: [
        'Art',
        'Education',
        'House',
        'Delivery',
        'Events',
        'Commercial',
        'Distribution',
        'Transport',
        'Technical_support',
        'Animals',
        'Beauty_bodycare',
        'Sport',
        'Health',
        'Fashion',
        'Web_service',
        'Others',
        'Network_multimedia',
        'Relocation',
        'Office_service',
        'Hotel_catering'
      ],
      containerProps: {
        lg: 12,
        md: 12,
        sm: 12,
        xs: 12
      }
    },
    {
      type: 'chip-input',
      label:
        'Préréquis (appuiyer sur la touche entrer pour ajouter un préréquis)',
      name: 'prerequisites',
      containerProps: {
        lg: 12,
        md: 12,
        sm: 12,
        xs: 12
      }
    },
    {
      type: 'info',
      info: <Typography variant="h3">Fréquence du travail</Typography>
    },
    {
      type: 'radio',
      label: 'Jour',
      name: 'frequencies',
      radioGroup: [
        {
          value: 'monday',
          label: 'Lundi'
        },
        {
          value: 'tuesday',
          label: 'Mardi'
        },
        {
          value: 'wednesday',
          label: 'Mercredi'
        },
        {
          value: 'thursday',
          label: 'jeudi'
        },
        {
          value: 'friday',
          label: 'Vendredi'
        },
        {
          value: 'saturday',
          label: 'Samedi'
        },
        {
          value: 'sunday ',
          label: 'Dimanche'
        }
      ],
      containerProps: {
        lg: 12,
        md: 12,
        sm: 12,
        xs: 12
      }
    },
    {
      type: 'info',
      info: <Typography variant="h6">Créneau horaire</Typography>
    },
    {
      type: 'custom-input',
      label: 'Créneau horaire',
      name: 'timeSlots_frequencies',
      containerProps: {
        lg: 12,
        md: 12,
        sm: 12,
        xs: 12
      }
    },
    {
      type: 'radio',
      label: "L'employeur choissira t-il le candidat lui même ?",
      name: 'chooseCandidate',
      required: true,
      radioGroup: [
        {
          value: 'Oui',
          label: 'Oui'
        },
        {
          value: 'Non',
          label: 'Non'
        }
      ],
      containerProps: {
        lg: 12,
        md: 12,
        sm: 12,
        xs: 12
      }
    },
    {
      type: 'radio',
      label: 'Le travail sera un CDD ou pas ?',
      name: 'isRegular',
      radioGroup: [
        {
          value: 'Oui',
          label: 'Oui'
        },
        {
          value: 'Non',
          label: 'Non'
        }
      ],
      containerProps: {
        lg: 12,
        md: 12,
        sm: 12,
        xs: 12
      }
    },
    {
      type: 'info',
      info: <Typography variant="h4">Liste des dates</Typography>,
      containerProps: {
        lg: 12,
        md: 12,
        sm: 12,
        xs: 12
      }
    },
    {
      type: 'date',
      label: 'Date ',
      name: 'day',
      containerProps: {
        lg: 6,
        md: 6,
        sm: 12,
        xs: 12
      }
    },
    {
      type: 'custom-input',
      label: 'Créneau horaire',
      name: 'timeSlots_day',
      containerProps: {
        lg: 12,
        md: 12,
        sm: 12,
        xs: 12
      }
    }
  ];

  return (
    <Formik
      initialValues={{
        title: 'title',
        town: 'town',
        street: 'street',
        slug: 'slug',
        reason: 'reason',
        referenceStreet: 'referenceStreet',
        description: 'description',
        state: 'in progress',
        isValid: true,
        nbplaces: 2,
        price: 1000,
        startDate: '2020-11-17',
        endDate:  '2020-11-20',
        employer: '',
        typeEmployer: 'particular',
        tags: ['Art', 'Education', 'House', 'Delivery'],
        prerequisites: ['pre1', 'pre2'],
        frequencies: 'monday',
        timeSlots_frequencies: [
          {
            startHour: '15:00',
            endHour: '16:00'
          },
          {
            startHour: '15:00',
            endHour: '16:00'
          }
        ],
        chooseCandidate: true,
        isRegular: true,
        day: '2020-11-15',
        timeSlots_day: [
          {
            startHour: '15:00',
            endHour: '16:00'
          },
          {
            startHour: '15:00',
            endHour: '16:00'
          }
        ]
      }}
      validationSchema={Yup.object().shape({
        town: Yup.string()
          .max(255)
          .required('La ville  est requise'),
        street: Yup.string()
          .max(255)
          .required('La rue  est requise'),
        title: Yup.string()
          .max(255)
          .required('Le titre est requis'),
        description: Yup.string()
          .max(255)
          .required('Le description est requise'),
        employer: Yup.string()
          .max(255)
          .required("L'employé est requis"),
        startDate: Yup.string()
          .max(255)
          .required('La date de debut est requise'),
        endDate: Yup.string()
          .max(255)
          .required('La date de fin est requise'),
        employerPayment: Yup.string()
          .max(255)
          .required('Ce champ est requis'),
        tags: Yup.string().required('Ce champ est requis')
      })}
      onSubmit={(values, { setSubmitting }) => {
        const data = {
          town: values.town,
          street: values.street,
          referenceStreet: values.referenceStreet,
          title: values.title,
          description: values.description,
          employer: values.employer.id,
          state: values.state,
          isValid: values.isValid === 'Oui' ? true :false,
          nbplaces: values.nbplaces,
          nbPlacesLeft: values.nbPlacesLeft,
          startDate: values.startDate,
          endDate: values.endDate,
          chooseCandidate: values.chooseCandidate=== 'Oui' ? true :false,
          frequency: {
            isRegular: values.isRegular === 'Oui' ? true :false,
            value_frequency: {
              day: values.frequencies,
              timeSlots: values.timeSlots_frequencies
            },
            listOfDates: {
              day: values.day,
              timeSlots: values.timeSlots_day
            }
          },
          employerPayment: values.employerPayment,
          price: values.price,
          prerequisites: values.prerequisites,
          slug: values.slug,
          tags: values.tags,
          nbViews: values.nbViews,
          typeEmployer: values.typeEmployer,
          reason: values.reason
        };

        dispatch(AddJobEffect(data, setLoading, enqueueSnackbar));
      }}
    >
      {({
        errors,
        handleBlur,
        handleChange,
        handleSubmit,
        isSubmitting,
        setFieldValue,
        touched,
        values
      }) => (
        <form onSubmit={handleSubmit}>
          <Grid container spacing={1}>
            {forms.map(({ type = 'text', ...form }) => (
              <Grid item {...(form.containerProps || {})}>
                {['text'].indexOf(type) !== -1 ? (
                  <>
                    <Box>
                      <Typography variant="h6">
                        {form.label}
                        {form.required && (
                          <Box component={'span'} color={'primary.main'}>
                            *
                          </Box>
                        )}
                      </Typography>{' '}
                    </Box>
                    <TextField
                      required={form.required || false}
                      error={Boolean(touched[form.name] && errors[form.name])}
                      fullWidth
                      helperText={touched[form.name] && errors[form.name]}
                      name={form.name}
                      margin="dense"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      type={form.type}
                      value={values[form.name]}
                      variant="outlined"
                      rows={form.row}
                      multiline={form.multiline}
                    />
                  </>
                ) : ['radio'].indexOf(type) !== -1 ? (
                  <>
                    <Box>
                      <Typography variant="h6">
                        {form.label}
                        {form.required && (
                          <Box component={'span'} color={'primary.main'}>
                            *
                          </Box>
                        )}
                      </Typography>{' '}
                    </Box>
                    <RadioGroup
                      row
                      aria-label={form.name}
                      name={form.name}
                      getOptionSelected={(option, value) =>
                        option.name === value.name
                      }
                      value={values[form.name]}
                      onChange={handleChange}
                    >
                      {form.radioGroup.map(radios => (
                        <FormControlLabel
                          color={'primary'}
                          value={radios.value}
                          control={<Radio size={15} />}
                          label={radios.label}
                        />
                      ))}
                    </RadioGroup>
                  </>
                ) : ['chip-input'].indexOf(type) !== -1 ? (
                  <>
                    <Box my={1}>
                      <Typography variant="h6">
                        {form.label}
                        {form.required && (
                          <Box component={'span'} color={'primary.main'}>
                            *
                          </Box>
                        )}
                      </Typography>{' '}
                    </Box>
                    <ChipInput
                      required={form.required}
                      size={'small'}
                      allowDuplicates={false}
                      defaultValue={values[form.name]}
                      error={Boolean(touched[form.name] && errors[form.name])}
                      fullWidth
                      helperText={touched[form.name] && errors[form.name]}
                      name={form.name}
                      onBlur={handleBlur}
                      onChange={chips => setFieldValue(form.name, chips)}
                      variant={'outlined'}
                    />
                  </>
                ) : ['info'].indexOf(type) !== -1 ? (
                  <>{form.info}</>
                ) : ['custom-input'].indexOf(type) !== -1 ? (
                  <>
                    <TimeSlots
                      name={form.name}
                      setFieldValue={setFieldValue}
                      values={values[form.name]}
                    />
                    {touched[form.name] && errors[form.name] && (
                      <Box mt={1}>
                        <FormHelperText error>
                          {errors[form.name]}
                        </FormHelperText>
                      </Box>
                    )}
                  </>
                ) : ['date'].indexOf(type) !== -1 ? (
                  <>
                    <Typography variant="h6">
                      {form.label}
                      {form.required && (
                        <Box component={'span'} color={'primary.main'}>
                          *
                        </Box>
                      )}
                    </Typography>{' '}
                    <DateField
                      helperText={touched[form.name] && errors[form.name]}
                      error={Boolean(touched[form.name] && errors[form.name])}
                      minDate={form.minDate}
                      maxDate={form.maxDate}
                      label={''}
                      fullWidth
                      value={values[form.name]}
                      onChange={content => {
                        setFieldValue(form.name, content);
                      }}
                      variant={'outlined'}
                    />
                    {touched[form.name] && errors[form.name] && (
                      <Box mt={1}>
                        <FormHelperText error>
                          {errors[form.name]}
                        </FormHelperText>
                      </Box>
                    )}
                  </>
                ) : ['auto-complete'].indexOf(type) !== -1 ? (
                  <>
                    <Box mt={1}>
                      <Typography variant="h6">
                        {form.label}
                        {form.required && (
                          <Box component={'span'} color={'primary.main'}>
                            *
                          </Box>
                        )}
                      </Typography>{' '}
                    </Box>
                    <Autocomplete
                      size={'small'}
                      id="country-select-demo"
                      options={form.datas}
                      autoHighlight
                      value={values[form.name]}
                      getOptionLabel={option => option.label}
                      onChange={(e, value) => {
                        console.log('value', value);
                        setFieldValue(form.name, value);
                      }}
                      renderOption={option => (
                        <React.Fragment>{option.label}</React.Fragment>
                      )}
                      renderInput={params => (
                        <TextField
                          {...params}
                          variant="outlined"
                          helperText={touched[form.name] && errors[form.name]}
                          error={Boolean(
                            touched[form.name] && errors[form.name]
                          )}
                          inputProps={{
                            ...params.inputProps,
                            autoComplete: 'new-password' // disable autocomplete and autofill
                          }}
                        />
                      )}
                    />
                  </>
                ) : ['list-check-boxes'].indexOf(type) !== -1 ? (
                  <>
                    <Box mt={1}>
                      <Typography variant="h6">
                        {form.label}
                        {form.required && (
                          <Box component={'span'} color={'primary.main'}>
                            *
                          </Box>
                        )}
                      </Typography>{' '}
                    </Box>
                    <Autocomplete
                      multiple
                      value={values[form.name]}
                      getOptionSelected={(option, value) => option === value}
                      options={form.datas}
                      disableCloseOnSelect
                      onChange={(e, value) => {
                        /*  setValues(value); */
                        setFieldValue(form.name, value);
                      }}
                      getOptionLabel={option => option}
                      renderOption={(option, { selected }) => (
                        <React.Fragment>
                          <Checkbox checked={selected} />
                          {option}
                        </React.Fragment>
                      )}
                      renderInput={params => (
                        <TextField
                          helperText={touched[form.name] && errors[form.name]}
                          error={Boolean(
                            touched[form.name] && errors[form.name]
                          )}
                          {...params}
                          variant="outlined"
                          error={touched[form.name] && errors[form.name]}
                        />
                      )}
                    />
                  </>
                ) : (
                  <div />
                )}
              </Grid>
            ))}
          </Grid>
          <Box my={2}>
            <Button
              color="primary"
              /*  disabled={isSubmitting} */
              // loading={loading}
              fullWidth
              size="large"
              type="submit"
              variant="contained"
            >
              se connecter
            </Button>
          </Box>
        </form>
      )}
    </Formik>
  );
}
