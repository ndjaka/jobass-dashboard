import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import CloseIcon from '@material-ui/icons/Close';
import Slide from '@material-ui/core/Slide';

import { Box, Container } from '@material-ui/core';
import JobForm2 from './jobForm2';

const useStyles = makeStyles(theme => ({
  appBar: {
    position: 'fixed',
   
  },
  title: {
    marginLeft: theme.spacing(2),
    flex: 1
  }
}));

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

export default function AddDialog({fetchData, open, onClose,type }) {
  const classes = useStyles();

  const handleClose = () => {
    onClose();
    fetchData();
  };

  console.log({ open });
  return (
    <div>
      <Dialog
        fullScreen
        open={open}
        onClose={handleClose}
        TransitionComponent={Transition}
      >
        <AppBar
          elevation={0}
          className={classes.appBar} abou>
          <Toolbar>
            <IconButton
              edge="start"
              color="inherit"
              onClick={handleClose}
              aria-label="close"
            >
              <CloseIcon />
            </IconButton>
            <Typography variant="h6" className={classes.title}>
            {  type === 'update' ? 'Modifier un job':'Ajouter un job'}
            </Typography>
            {/* <Button autoFocus color="inherit" onClick={handleClose}>
              Ajouter
            </Button> */}
          </Toolbar>
        </AppBar>
        <Box mt={12} />
        <Container maxWidth={'md'}>
          <JobForm2 onCloseForm={handleClose} type={type} data={open}/>
        </Container>
      </Dialog>
    </div>
  );
}
