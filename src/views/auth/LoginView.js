import React, { useState } from 'react';
import { Link as RouterLink, useNavigate } from 'react-router-dom';
import * as Yup from 'yup';
import { Formik } from 'formik';
import {
  Box,
  Button,
  Container,
  Grid,
  Link,
  TextField,
  Typography,
  makeStyles,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  FormHelperText
} from '@material-ui/core';


import { useDispatch } from 'react-redux';
import { useSnackbar } from 'notistack';

import Page from '../../components/Page';
import { loginEffect } from '../../store/effects/AuthEffects';


const useStyles = makeStyles(theme => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    height: '100%',
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3)
  }
}));

const LoginView = () => {
  const classes = useStyles();
  const navigate = useNavigate();
  const [loading, setLoading] = useState(false);
  const dispatch = useDispatch();
  const { enqueueSnackbar } = useSnackbar();
  console.log('process',process.env.API_URL)
  return (
    <Page className={classes.root} title="Login">
      <Box
        display="flex"
        flexDirection="column"
        height="100%"
        justifyContent="center"
      >
        <Container maxWidth="sm">
          <Formik
            initialValues={{
              email: 'jordanet@jobaas.fr',
              password: 'string9S#',
              user: 'administrator'
            }}
            validationSchema={Yup.object().shape({
              email: Yup.string()
                .email('email invalide')
                .max(255)
                .required("L'adresse email est requise"),
              password: Yup.string()
                .max(255)
                .required('Le mot de passe est requis'),
              user: Yup.string()
                .max(255)
                .required("L'utilisateur est requis")
            })}
            onSubmit={(values) => {
              dispatch(
                loginEffect(
                  values,
                  setLoading,
                  enqueueSnackbar,
                  navigate
                )
              );
            }}
          >
            {({
              errors,
              handleBlur,
              handleChange,
              handleSubmit,
              isSubmitting,
              touched,
              values
            }) => (
              <form onSubmit={handleSubmit}>
                <Box mb={3}>
                  <Typography color="textPrimary" variant="h2">
                    JOBAAS
                  </Typography>
                  <Typography
                    color="textSecondary"
                    gutterBottom
                    variant="body2"
                  >
                    Se connecter à la plate forme
                  </Typography>
                </Box>

                <TextField
                  error={Boolean(touched.email && errors.email)}
                  fullWidth
                  helperText={touched.email && errors.email}
                  label="Email"
                  margin="normal"
                  name="email"
                  onBlur={handleBlur}
                  onChange={handleChange}
                  type="email"
                  value={values.email}
                  variant="outlined"
                />
                <TextField
                  error={Boolean(touched.password && errors.password)}
                  fullWidth
                  helperText={touched.password && errors.password}
                  label="Mot de passe"
                  margin="normal"
                  name="password"
                  onBlur={handleBlur}
                  onChange={handleChange}
                  type="password"
                  value={values.password}
                  variant="outlined"
                />
                <FormControl
                  error={Boolean(touched.user && errors.user)}
                  fullWidth
                  margin="normal"
                  variant="outlined"
                >
                  <InputLabel id="demo-simple-select-outlined-label">
                    Utilisateur
                  </InputLabel>
                  <Select
                    labelId="demo-simple-select-outlined-label"
                    id="demo-simple-select-outlined"
                    value={values.user}
                    name="user"
                    onBlur={handleBlur}
                    onChange={handleChange}
                    label="Utilisateur"
                  >
                    <MenuItem value="">
                      <em></em>
                    </MenuItem>
                    {[
                      {
                        id: 'particular',
                        value: 'Particulier'
                      },
                      {
                        id: 'administrator',
                        value: 'Administrateur'
                      },
                      {
                        id: 'company',
                        value: 'Compagnie'
                      }
                    ].map(item => (
                      <MenuItem value={item.id}>{item.value}</MenuItem>
                    ))}
                  </Select>
                  <FormHelperText>{touched.user && errors.user}</FormHelperText>
                </FormControl>
                <Box my={2}>
                  <Button
                    color="primary"
                    disabled={isSubmitting}
                   // loading={loading}
                    fullWidth
                    size="large"
                    type="submit"
                    variant="contained"
                  >
                    se connecter
                  </Button>
                </Box>
              </form>
            )}
          </Formik>
        </Container>
      </Box>
    </Page>
  );
};

export default LoginView;
