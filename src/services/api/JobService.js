import config from '../../config';
import BaseService from './BaseService';
import { adminsUrl, JobsUrl } from './urls';

const apiUrl = config.api_url;

class JobService {
  static getJobs(url) {
    return BaseService.getRequest(`${apiUrl}${url}`, true);
  }

  static addJobs(data) {
    return BaseService.postRequest(JobsUrl.ADD_JOB, data, true);
  }
  
  static getEmployerByName() {
    return BaseService.getRequest(adminsUrl.GET_EMPLOYER_BY_NAME, true);
  }

  static getSpecificJob(id) {
    return BaseService.getRequest(JobsUrl.GET_JOB(id), true);
  }
  
  static updateJob(id,data) {
    return BaseService.putRequest(adminsUrl.UPDATE_JOB(id),data, true);
  }
}

JobService.publicName = 'JobService';
export default JobService;
