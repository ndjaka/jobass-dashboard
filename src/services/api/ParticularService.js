import config from '../../config';
import BaseService from './BaseService';
import { adminsUrl } from './urls';


const apiUrl = config.api_url;

class ParticularService {
  static getParticular(url) {
    return BaseService.getRequest(`${apiUrl}${url}`, true);
  }

 static addParticular(data) {
    return BaseService.postRequest(adminsUrl.ADD_PARTICULAR, data, true);
 }
  
  /*static getEmployerByName() {
    return BaseService.getRequest(adminsUrl.GET_EMPLOYER_BY_NAME, true);
  }

  static getSpecificJob(id) {
    return BaseService.getRequest(JobsUrl.GET_JOB(id), true);
  } */
}

ParticularService.publicName = 'ParticularService';
export default ParticularService;
