import config from "../../config";


export const apiUrl = config.api_url;

export const AuthUrl = {
  LOGIN:(user)=>`${apiUrl}auth/login/${user}`,
};

export const JobsUrl = {
  GET_JOBS: (limit, page, keyword) => `${apiUrl}administrator/me/job?limit=${limit}&page=${page}&keyword=${keyword}`,
  ADD_JOB:`${apiUrl}job`,
  GET_JOB:(id)=>`${apiUrl}job/${id}`,
};

export const adminsUrl = {
  GET_EMPLOYER_BY_NAME: `${apiUrl}administrator/search/particular?name=`,
  UPDATE_JOB: (id) => `${apiUrl}job/${id}`,
  ADD_PARTICULAR:`${apiUrl}particular`
  
};
