import BaseService from './BaseService'
import {AuthUrl} from "./urls";

class AuthService {


    static login(data,user) {
        return BaseService.postRequest(AuthUrl.LOGIN(user), data, false);
    };

}

AuthService.publicName = 'AuthService';
export default AuthService


