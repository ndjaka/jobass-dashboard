export const SET_JOBS = "SET_JOBS";



export const setJobs = (payload) => dispatch =>

  dispatch({
    type: SET_JOBS,
    payload: payload
  });

