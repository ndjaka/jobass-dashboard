export const SET_PARTICULAR = "SET_PARTICULAR";



export const setParticular = (payload) => dispatch =>

  dispatch({
    type: SET_PARTICULAR,
    payload: payload
  });

