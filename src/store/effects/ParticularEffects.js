import { ParticularService } from '../../services/api';
import { setParticular } from '../actions/ParticularAction';


export const getListParticularEffect = (
  url,
  setLoading,
  enqueueSnackbar
) => async dispatch => {
  setLoading(true);
  return ParticularService.getParticular(url)
    .then(async res => {
      if (res.status === 201 || res.status === 200) {
        const result = await res.json();
        dispatch(setParticular(result));
      } else {
        const { message } = await res.json();
        enqueueSnackbar(message, { variant: 'warning' });
      }
    })
    .catch(err => {
      console.log(err);
      enqueueSnackbar('verifiez votre connexion internet', {
        variant: 'error'
      });
    })
    .finally(() => setLoading(false));
};

export const addParticular = (
  data,
  setLoading,
  enqueueSnackbar
)  => {
  setLoading(true);
  return ParticularService.addParticular(data)
    .then(async res => {
      if (res.status === 201 || res.status === 200) {
        
        const { message } = await res.json();
        enqueueSnackbar(message, { variant: 'success' });
      } else {
        const { message } = await res.json();
        enqueueSnackbar(message, { variant: 'warning' });
      }
    })
    .catch(err => {
      console.log(err);
      enqueueSnackbar('verifiez votre connexion internet', {
        variant: 'error'
      });
    })
    .finally(() => setLoading(false));
};

