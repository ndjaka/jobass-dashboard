
import { AuthService } from '../../services/api';
import { login } from '../actions/AuthAction';

export const loginEffect = (
  data,
  setLoading,
  enqueueSnackbar,
  navigate
) => async (dispatch, getState) => {
  const {session} = getState().session;
  
  setLoading(true);
  return AuthService.login(
    { email: data.email, password: data.password },
    data.user
  )
    .then(async res => {
      if (res.status === 201) {
        const {
          data: { accessToken }
        } = await res.json();
        dispatch(login({ ...session, token: accessToken }));
        navigate('/app/jobs', { replace: true });
      } else {
        const { message } = await res.json();
        enqueueSnackbar(message, { variant: 'warning' });
      }
    })
    .catch(err => {
      console.log(err);
      enqueueSnackbar('verifiez votre connexion internet', {
        variant: 'error'
      });
    })
    .finally(() => setLoading(false));
};
