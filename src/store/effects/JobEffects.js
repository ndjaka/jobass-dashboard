import { JobService } from '../../services/api';
import { setJobs } from '../actions/JobsAction';

export const getListJobsEffect = (
  url,
  setLoading,
  enqueueSnackbar
) => async dispatch => {
  setLoading(true);
  return JobService.getJobs(url)
    .then(async res => {
      if (res.status === 201 || res.status === 200) {
        const result = await res.json();
        dispatch(setJobs(result));
      } else {
        const { message } = await res.json();
        enqueueSnackbar(message, { variant: 'warning' });
      }
    })
    .catch(err => {
      console.log(err);
      enqueueSnackbar('verifiez votre connexion internet', {
        variant: 'error'
      });
    })
    .finally(() => setLoading(false));
};

export const AddJobEffect = (
  data,
  setLoading,
  enqueueSnackbar,
  onCloseForm
) => async dispatch => {
  setLoading(true);
  return JobService.addJobs(data)
    .then(async res => {
      if (res.status === 201 || res.status === 200) {
        const { message } = await res.json();
        onCloseForm();
        enqueueSnackbar(message, { variant: 'success' });
      } else {
        const { message } = await res.json();
        enqueueSnackbar(message, { variant: 'warning' });
      }
    })
    .catch(err => {
      console.log(err);
      enqueueSnackbar('verifiez votre connexion internet', {
        variant: 'error'
      });
    })
    .finally(() => setLoading(false));
  };

export const updateJobEffect = (
  data,
  id,
  setLoading,
  enqueueSnackbar,
  onCloseForm
) => async dispatch => {
  console.log(id,data)
  setLoading(true);
  return JobService.updateJob(id,data)
    .then(async res => {
      if (res.status === 201 || res.status === 200) {
        const { message } = await res.json();
        onCloseForm();
        enqueueSnackbar(message, { variant: 'success' });
      } else {
        const { message } = await res.json();
        enqueueSnackbar(message, { variant: 'warning' });
      }
    })
    .catch(err => {
      console.log(err);
      enqueueSnackbar('verifiez votre connexion internet', {
        variant: 'error'
      });
    })
    .finally(() => setLoading(false));
};

export const getEmployerEffect = (onSuccess, enqueueSnackbar) => {
  return JobService.getEmployerByName()
    .then(async res => {
      if (res.status === 201 || res.status === 200) {
        const { data } = await res.json();
        onSuccess(data);
      } else {
        const { message } = await res.json();
        enqueueSnackbar(message, { variant: 'warning' });
      }
    })
    .catch(err => {
      console.log(err);
      enqueueSnackbar('verifiez votre connexion internet', {
        variant: 'error'
      });
    })
    
};

export const getJobEffect = (id , onSuccess, enqueueSnackbar) => {
  return JobService.getSpecificJob(id)
    .then(async res => {
      if (res.status === 201 || res.status === 200) {
        const { data } = await res.json();
        onSuccess(data);
      } else {
        const { message } = await res.json();
        enqueueSnackbar(message, { variant: 'warning' });
      }
    })
    .catch(err => {
      console.log(err);
      enqueueSnackbar('verifiez votre connexion internet', {
        variant: 'error'
      });
    })
    
};

