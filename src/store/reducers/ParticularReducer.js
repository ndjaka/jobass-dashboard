import * as actionTypes from '../actions';
const initialState = {
  data: {},
};

const ParticularReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.SET_PARTICULAR : {
      return {
       
        ...action.payload
      };
    }

    default: {
      return state;
    }
  }
};

export default ParticularReducer;
