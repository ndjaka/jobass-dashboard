import * as actionTypes from '../actions';
const initialState = {
  data: {},
};

const JobReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.SET_JOBS: {
      return {
       
        ...action.payload
      };
    }

    default: {
      return state;
    }
  }
};

export default JobReducer;
