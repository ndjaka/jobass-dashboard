import { combineReducers } from "redux";
import AuthReducer from "./reducers/AuthReducers";
import JobReducer from "./reducers/JobReducers";
import ParticularReducer from "./reducers/ParticularReducer";

const rootReducer = combineReducers({
  session: AuthReducer,
  jobs:JobReducer,
  particular:ParticularReducer
});

export default rootReducer;
