import {
  AlertCircle as AlertCircleIcon,
  BarChart as BarChartIcon,
  Lock as LockIcon,
  Settings as SettingsIcon,
  ShoppingBag as ShoppingBagIcon,
  User as UserIcon,
  UserPlus as UserPlusIcon,
  Users as UsersIcon
} from 'react-feather';

import WorkOutlineIcon from '@material-ui/icons/WorkOutline';
import PersonIcon from '@material-ui/icons/Person';

const items = [
/*   {
    href: '/app/dashboard',
    icon: BarChartIcon,
    title: 'Dashboard'
  }, */
  {
    href: '/app/jobs',
    icon: WorkOutlineIcon,
    title: 'Jobs'
  },
  {
    href: '/app/particulars',
    icon: PersonIcon,
    title: 'Particuliers'
  },
 /*  {
    href: '/app/products',
    icon: ShoppingBagIcon,
    title: 'Products'
  },
  {
    href: '/app/account',
    icon: UserIcon,
    title: 'Account'
  },
  {
    href: '/app/settings',
    icon: SettingsIcon,
    title: 'Settings'
  },
  {
    href: '/login',
    icon: LockIcon,
    title: 'Login'
  },
  {
    href: '/register',
    icon: UserPlusIcon,
    title: 'Register'
  },
  {
    href: '/404',
    icon: AlertCircleIcon,
    title: 'Error'
  } */
];

export default items;
