import React from 'react';
import {
  Box,
  Button,
  FormControl,
  Grid,
  IconButton,
  MenuItem,
  Select,
  Typography
} from '@material-ui/core';
import RemoveIcon from '@material-ui/icons/Remove';

function Languages({ name, setFieldValue, values }) {
 /*  const [langs, setLangs] = React.useState([
    {
      value: '',
      level: ''
    }
  ]); */

  const langues = [
    {
      value: 'English',
      label: 'Anglais'
    },
    {
      value: 'French',
      label: 'Francais'
    },
    {
      value: 'Italian',
      label: 'Italien'
    },
    {
      value: 'Spanish',
      label: 'Espagnol'
    },
    {
      value: 'Chinese',
      label: 'Chinois'
    }
  ];

  const levels = [
    {
      value: 'beginner',
      label: 'Debutant'
    },
    {
      value: 'intermediate',
      label: 'Intermediaire'
    },
    {
      value: 'high',
      label: 'high'
    },
    {
      value: 'professional',
      label: 'professional'
    },
    {
      value: 'maternal',
      label: 'maternal'
    }
  ];

  return (
    <div>
      {values &&
        values.map((lang, index) => (
          <Grid container spacing={2}>
            <Grid item lg={5} md={6} sm={12} xs={12}>
              <Typography variant="subtitle2">Langue</Typography>
              <FormControl variant="outlined" fullWidth>
                <Select
                  onChange={e => {
                    const langUpdated = values.map((lang, id) =>
                      id === index ? { ...lang, value: e.target.value } : lang
                    );
                    setFieldValue(name, langUpdated);
                  }}
                  value={lang.value}
                >
                  {langues.map(item => (
                    <MenuItem value={item.value}>{item.label}</MenuItem>
                  ))}
                </Select>
              </FormControl>
            </Grid>
            <Grid item lg={5} md={6} sm={12} xs={12}>
              <Typography variant="subtitle2">Niveau</Typography>
              <FormControl variant="outlined" fullWidth>
                <Select
                  onChange={e => {
                    const levelUpdated = values.map((lang, id) =>
                      id === index ? { ...lang, level: e.target.value } : lang
                    );
                    setFieldValue(name, levelUpdated);
                  }}
                  value={lang.level}
                >
                  {levels.map(item => (
                    <MenuItem value={item.value}>{item.label}</MenuItem>
                  ))}
                </Select>
              </FormControl>
            </Grid>
            <Grid item lg={1} md={6} sm={12} xs={12}>
              <Grid display="flex" justify={'center'} alignItems={'center'}>
                <IconButton
                  color={'primary'}
                  onClick={() => {
                    const langsUpdated = values.filter(
                      (slot, id) => id != index
                    );
                    setFieldValue(name, langsUpdated);
                  }}
                >
                  <RemoveIcon />
                </IconButton>
              </Grid>
            </Grid>
          </Grid>
        ))}

      <Box mt={1}>
        <Button
          variant="outlined"
          color="primary"
          onClick={() =>
            setFieldValue(name,[
              ...values,
              {
                value: '',
                level: ''
              }
            ])
          }
        >
          + Ajouter une langue
        </Button>
      </Box>
    </div>
  );
}

Languages.propTypes = {};

export default Languages;
