import React, { useRef, useState } from 'react';
import {
  InputAdornment,
  IconButton,
  withStyles,
  Box,
  capitalize
} from '@material-ui/core';
import moment from 'moment';
import { DatePicker, MuiPickersUtilsProvider } from '@material-ui/pickers';
import MomentUtils from '@date-io/moment';
import { TextField } from '@material-ui/core';
import 'moment/locale/fr';
import { CalendarToday } from '@material-ui/icons';

const CssTextField = withStyles({
  root: {
    borderRadius: '10px !important'
  }
})(TextField);

export default function DateField(props) {
  const fileInputRef = useRef(null);
  const {
    onChange,
    error,
    value,
    maxDate,
    minDate,
    className,
    fullWidth,
    label,
    helperText,
    ...rest
  } = props;
  const [date, setDate] = useState(new Date());
  const [open, setOpen] = useState(false);

  return (
    <div className={className}>
      <Box component={'div'} onClick={() => setOpen(true)}>
        <CssTextField
          variant={'outlined'}
          fullWidth={fullWidth}
          helperText={helperText}
          label={capitalize(label)}
          error={error}
          placeholder={'DD-MM-YYYY'}
          InputProps={{
            endAdornment: (
              <InputAdornment position="end">
                <IconButton edge="end" onClick={() => setOpen(true)}>
                  <CalendarToday color={'primary'} />
                </IconButton>
              </InputAdornment>
            )
          }}
          value={value}
          type={'text'}
          {...rest}
        />
      </Box>

      <div style={{ display: 'none' }}>
        <MuiPickersUtilsProvider
          libInstance={moment}
          utils={MomentUtils}
          locale={'fr'}>
          <DatePicker
            maxDate={maxDate && maxDate}
            minDate={minDate && minDate}
            open={open}
            onClose={() => setOpen(false)}
            ref={fileInputRef}
            value={date}
            onChange={(date) => {
              const newDate = date._d;
              const m = moment(newDate);
              onChange(m.format('YYYY-MM-DD'));
              setDate(date._d);
            }}
            inputVariant="outlined"
            views={['date', 'month', 'year']}
            format="YYYY-MM-DD"
          />
        </MuiPickersUtilsProvider>
      </div>
    </div>
  );
}
