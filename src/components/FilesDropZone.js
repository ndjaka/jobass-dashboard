/* eslint-disable react/no-array-index-key */
import React, { FunctionComponent, useCallback, useState } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { useDropzone } from 'react-dropzone';
import {
  Box,
  Button,
  Card,
  CardContent,
  Link,
  makeStyles,
  Paper,
  Typography,
  useMediaQuery,
  useTheme
} from '@material-ui/core';


const useStyles = makeStyles((theme) => ({
  root: {},
  dropZone: {
    border: `1px dashed ${theme.palette.divider}`,
    padding: '25px 47px',
    outline: 'none',
    display: 'flex',
    justifyContent: 'center',
    flexWrap: 'wrap',
    alignItems: 'center',
    '&:hover': {
      backgroundColor: theme.palette.action.hover,
      opacity: 0.5,
      cursor: 'pointer'
    }
  },
  dragActive: {
    backgroundColor: theme.palette.action.active,
    opacity: 0.5
  },
  image: {
    width: 130
  },
  image2: {
    width: 130
  },
  info: {
    marginTop: theme.spacing(1)
  },
  list: {
    maxHeight: 320
  },
  actions: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
    display: 'flex',
    justifyContent: 'flex-end',
    '& > * + *': {
      marginLeft: theme.spacing(2)
    }
  }
}));



const FilesDropzone = ({
  className,
  defaultImgSrc,
  name,
  onChange,
  field: { show_paper = true, ...field },
  imageUnder = false,
  ...rest
}) => {
  const classes = useStyles();
  const [files, setFiles] = useState([]);

  const [newSource, setNewSource] = useState(defaultImgSrc);

  const handleDrop = useCallback(
    (acceptedFiles) => {
      setFiles((prevFiles) => [...prevFiles].concat(acceptedFiles));
      let data = new FormData();
      data.append(name, acceptedFiles[0]);
      onChange(data);
      setNewSource(URL.createObjectURL(acceptedFiles[0]));
    },
    [onChange, name]
  );

  const handleRemoveAll = () => {
    setFiles([]);
    const src = defaultImgSrc ? defaultImgSrc : '';
    onChange({});
    setNewSource(src);
  };

  const { getRootProps, getInputProps, isDragActive } = useDropzone({
    onDrop: handleDrop
  });
  const theme = useTheme();
  const phone = useMediaQuery(theme.breakpoints.down('xs'));

  const input = () => (
    <div className={clsx(classes.root, className)} {...rest}>
      <div
        className={clsx({
          [classes.dropZone]: true,
          [classes.dragActive]: isDragActive
        })}
        style={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: imageUnder ? '' : 'space-between',
          flexWrap: phone ? 'wrap' : 'nowrap'
        }}
        {...getRootProps()}>
        <input {...getInputProps()} />
        <div>
          {imageUnder ? (
            <img alt="Select file" className={classes.image2} src={newSource} />
          ) : (
            <img
              alt="Select file"
              className={classes.image}
              src="https://static.actu.fr/uploads/2020/05/presentation-avatars-fb.jpg"
            />
          )}
        </div>
        <Box component={'div'} textAlign={'center'}>
          {field.label ? (
            field.label
          ) : (
            <React.Fragment>
              <Typography gutterBottom variant="h3">
                Télécharger votre Logo
              </Typography>
              <Typography variant="h5">
                (Taille recommandé 512x512 px)
              </Typography>
              <Box mt={2}>
                <Typography color="textPrimary" variant="body1">
                  glisser votre image ici ou télécharger depuis{' '}
                  <Link underline="always">mon ordinateur</Link>{' '}
                </Typography>
              </Box>
            </React.Fragment>
          )}
        </Box>
      </div>
      <Box component={'div'} textAlign={'center'}>
        {imageUnder ? (
          <div />
        ) : (
          <div>
            {newSource && (
              <img
                src={newSource ? newSource : '/static/images/default_pp.svg'}
                style={{
                  minHeight: 100,
                  minWidth: 100,
                  maxWidth: '100%',
                  borderRadius: 2,
                  padding: '25px 47px'
                }}
                alt=""
              />
            )}
          </div>
        )}
      </Box>
      {files.length > 0 && (
        <>
          <div className={classes.actions}>
            <Button onClick={handleRemoveAll} size="small">
              Annuler
            </Button>
          </div>
        </>
      )}
    </div>
  );

  return (
    <Box>
      <Card elevation={show_paper ? 2 : 0}>
        <CardContent style={show_paper ? {} : { padding: 0 }}>
          {show_paper ? <Paper variant="outlined">{input()}</Paper> : input()}
        </CardContent>
      </Card>
    </Box>
  );
};

FilesDropzone.propTypes = {
  className: PropTypes.string
};

export default FilesDropzone;
