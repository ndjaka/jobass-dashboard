import {
  Box,
  Button,
  Divider,
  Grid,
  IconButton,
  TextField,
  Typography
} from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import RemoveIcon from '@material-ui/icons/Remove';
import DateField from './DateField';

export default function PonctualDate({ name, setFieldValue, values }) {
  //const [slots, setSlots] = useState(values);

  return (
    <div>
      {values &&
        values.map((slot, index) => (
          <Grid container key={index} spacing={2}>
            <Grid item lg={3} md={6} sm={12} xs={12}>
              <Box my={1}>
                <Typography variant="h6">heure de debut</Typography>{' '}
              </Box>
              <DateField
                placeholder={'Select date'}
                margin="dense"
                label={''}
                fullWidth
                value={slot.day}
                onChange={content => {
                  const slotUpdated = values.map((slot, id) =>
                    id === index ? { ...slot, day: content } : slot
                  );
                  //  setSlots(slotUpdated);
                  setFieldValue(name, slotUpdated);
                }}
                variant={'outlined'}
              />
            </Grid>
            <Grid item lg={4} md={6} sm={12} xs={12}>
              <Box my={1}>
                <Typography variant="h6">heure de debut</Typography>{' '}
              </Box>
              <TextField
                variant="outlined"
                margin="dense"
                type="time"
                required={true}
                fullWidth
                value={slot.startHour}
                onChange={e => {
                  const slotUpdated = values.map((slot, id) =>
                    id === index ? { ...slot, startHour: e.target.value } : slot
                  );
                  //  setSlots(slotUpdated);
                  setFieldValue(name, slotUpdated);
                }}
              />
            </Grid>
            <Grid item lg={4} md={6} sm={12} xs={12}>
              <Box my={1}>
                <Typography variant="h6">heure de fin</Typography>{' '}
              </Box>
              <TextField
                variant="outlined"
                margin="dense"
                type="time"
                required={true}
                fullWidth
                value={slot.endHour}
                onChange={e => {
                  const slotUpdated = values.map((slot, id) =>
                    id === index ? { ...slot, endHour: e.target.value } : slot
                  );
                  setFieldValue(name, slotUpdated);
                }}
              />
            </Grid>
            <Grid item lg={1} md={6} sm={12} xs={12}>
              <Grid display="flex" justify={'center'} alignItems={'center'}>
                <IconButton
                  color={'primary'}
                  onClick={() => {
                    const slotUpdated = values.filter(
                      (slot, id) => id != index
                    );
                    setFieldValue(name, slotUpdated);
                  }}
                >
                  <RemoveIcon />
                </IconButton>
              </Grid>
            </Grid>
          </Grid>
        ))}

      <Box mt={1}>
        <Button
          variant="outlined"
          color="primary"
          onClick={() =>
            /* setSlots([
              ...slots,
              {
                startHour: '',
                endHour: ''
              }
            ]) */
            setFieldValue(name, [
              ...values,
              {
                startHour: '',
                endHour: ''
              }
            ])
          }
        >
          + Ajouter un créneau
        </Button>
      </Box>
    </div>
  );
}
