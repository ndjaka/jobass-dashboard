import {
  Grid,
  TextField,
  Typography,
  makeStyles,
  Box,
  RadioGroup,
  FormControlLabel,
  Radio,
  Checkbox,
  FormHelperText,
  FormControl,
  Select,
  MenuItem
} from '@material-ui/core';
import { Formik } from 'formik';
import ChipInput from 'material-ui-chip-input';
import React from 'react';
import * as Yup from 'yup';
import { Autocomplete } from '@material-ui/lab';
import TimeSlots from './TimeSlots';
import DateField from './DateField';
import PonctualDate from './PonctualTime';
import Languages from './Languages';
import FilesDropzone from './FilesDropZone';

export default function FormBuilder({
  Field,
  initialValues,
  validationSchema,
  onSubmit,
  renderSubmit
}) {
  return (
    <Formik
      initialValues={initialValues}
      validationSchema={Yup.object().shape(validationSchema)}
      onSubmit={(values, actions) => {
        onSubmit(values, actions);
      }}
    >
      {({
        values,
        handleBlur,
        handleChange,
        setFieldValue,
        errors,
        setErrors,
        touched,
        setFieldTouched,
        isSubmitting,
        handleSubmit
      }) => (
        <form onSubmit={handleSubmit}>
          <Grid container spacing={1}>
            {Field.map(({ type = 'text', ...form }) => (
              <Grid item {...(form.containerProps || {})}>
                {['number', 'email', 'password'].indexOf(type) !== -1 ? (
                  <>
                    <Box>
                      <Typography variant="h6">
                        {form.label}
                        {form.required && (
                          <Box component={'span'} color={'primary.main'}>
                            *
                          </Box>
                        )}
                      </Typography>{' '}
                    </Box>
                    <TextField
                      required={form.required || false}
                      error={Boolean(touched[form.name] && errors[form.name])}
                      fullWidth
                      helperText={touched[form.name] && errors[form.name]}
                      name={form.name}
                      margin="dense"
                      onBlur={handleBlur}
                      onChange={handleChange}
                      type={form.type}
                      value={values[form.name]}
                      variant="outlined"
                      rows={form.row}
                      multiline={form.multiline}
                    />
                  </>
                ) : ['radio'].indexOf(type) !== -1 ? (
                  <div style={{ display: !form.show ? 'block' : 'none' }}>
                    <Box>
                      <Typography variant="h6">
                        {form.label}
                        {form.required && (
                          <Box component={'span'} color={'primary.main'}>
                            *
                          </Box>
                        )}
                      </Typography>{' '}
                    </Box>
                    <RadioGroup
                      row
                      aria-label={form.name}
                      name={form.name}
                      getOptionSelected={(option, value) =>
                        option.name === value.name
                      }
                      value={values[form.name]}
                      onChange={e => {
                        handleChange(e);
                        form.changeState && form.changeState(e.target.value);
                      }}
                    >
                      {form.radioGroup.map(radios => (
                        <FormControlLabel
                          color={'primary'}
                          value={radios.value}
                          control={<Radio size={15} />}
                          label={radios.label}
                        />
                      ))}
                    </RadioGroup>
                  </div>
                ) : ['chip-input'].indexOf(type) !== -1 ? (
                  <>
                    <Box my={1}>
                      <Typography variant="h6">
                        {form.label}
                        {form.required && (
                          <Box component={'span'} color={'primary.main'}>
                            *
                          </Box>
                        )}
                      </Typography>{' '}
                    </Box>
                    <ChipInput
                      required={form.required}
                      size={'small'}
                      allowDuplicates={false}
                      defaultValue={values[form.name]}
                      error={Boolean(touched[form.name] && errors[form.name])}
                      fullWidth
                      helperText={touched[form.name] && errors[form.name]}
                      name={form.name}
                      margin="dense"
                      onBlur={handleBlur}
                      onChange={chips => setFieldValue(form.name, chips)}
                      variant={'outlined'}
                    />
                  </>
                ) : ['info'].indexOf(type) !== -1 ? (
                  <>{form.info}</>
                ) : ['custom-input'].indexOf(type) !== -1 ? (
                  <div style={{ display: !form.show ? 'block' : 'none' }}>
                    <TimeSlots
                      name={form.name}
                      setFieldValue={setFieldValue}
                      values={values[form.name]}
                    />
                    {touched[form.name] && errors[form.name] && (
                      <Box mt={1}>
                        <FormHelperText error>
                          {errors[form.name]}
                        </FormHelperText>
                      </Box>
                    )}
                  </div>
                ) : ['date'].indexOf(type) !== -1 ? (
                  <div style={{ display: !form.show ? 'block' : 'none' }}>
                    <Typography variant="h6">
                      {form.label}
                      {form.required && (
                        <Box component={'span'} color={'primary.main'}>
                          *
                        </Box>
                      )}
                    </Typography>{' '}
                    <DateField
                      helperText={touched[form.name] && errors[form.name]}
                      error={Boolean(touched[form.name] && errors[form.name])}
                      minDate={form.minDate}
                      maxDate={form.maxDate}
                      label={''}
                      fullWidth
                      value={values[form.name]}
                      onChange={content => {
                        setFieldValue(form.name, content);
                      }}
                      variant={'outlined'}
                    />
                    {touched[form.name] && errors[form.name] && (
                      <Box mt={1}>
                        <FormHelperText error>
                          {errors[form.name]}
                        </FormHelperText>
                      </Box>
                    )}
                  </div>
                ) : ['auto-complete'].indexOf(type) !== -1 ? (
                  <>
                    <Box mt={1}>
                      <Typography variant="h6">
                        {form.label}
                        {form.required && (
                          <Box component={'span'} color={'primary.main'}>
                            *
                          </Box>
                        )}
                      </Typography>{' '}
                    </Box>
                    <Autocomplete
                      size={'small'}
                      id="country-select-demo"
                      value={values[form.name]}
                      
                      options={form && form.datas}
                      getOptionLabel={option => option.label}
                      onChange={(e, value) => {
                        console.log('value', value);
                        setFieldValue(form.name, value);
                      }}
                      renderOption={option => (
                        <React.Fragment>{option.label}</React.Fragment>
                      )}
                      renderInput={params => (
                        <TextField
                          {...params}
                          variant="outlined"
                          helperText={touched[form.name] && errors[form.name]}
                          error={Boolean(
                            touched[form.name] && errors[form.name]
                          )}
                          inputProps={{
                            ...params.inputProps,
                            autoComplete: 'new-password' // disable autocomplete and autofill
                          }}
                        />
                      )}
                    />
                  </>
                ) : ['list-check-boxes'].indexOf(type) !== -1 ? (
                  <>
                    <Box mt={1}>
                      <Typography variant="h6">
                        {form.label}
                        {form.required && (
                          <Box component={'span'} color={'primary.main'}>
                            *
                          </Box>
                        )}
                      </Typography>{' '}
                    </Box>
                    <Autocomplete
                      multiple
                      value={values[form.name]}
                      getOptionSelected={(option, value) => option === value}
                      options={form.datas}
                      disableCloseOnSelect
                      onChange={(e, value) => {
                        /*  setValues(value); */
                        setFieldValue(form.name, value);
                      }}
                      getOptionLabel={option => option}
                      renderOption={(option, { selected }) => (
                        <React.Fragment>
                          <Checkbox checked={selected} />
                          {option}
                        </React.Fragment>
                      )}
                      renderInput={params => (
                        <TextField
                          helperText={touched[form.name] && errors[form.name]}
                          error={Boolean(
                            touched[form.name] && errors[form.name]
                          )}
                          {...params}
                          variant="outlined"
                          error={touched[form.name] && errors[form.name]}
                        />
                      )}
                    />
                  </>
                ) : ['ponctual-date'].indexOf(type) !== -1 ? (
                  <div style={{ display: !form.show ? 'block' : 'none' }}>
                    <Box>
                      <Typography variant="h6">
                        {form.label}
                        {form.required && (
                          <Box component={'span'} color={'primary.main'}>
                            *
                          </Box>
                        )}
                      </Typography>{' '}
                    </Box>
                    <PonctualDate
                      name={form.name}
                      setFieldValue={setFieldValue}
                      values={values[form.name]}
                    />
                    {touched[form.name] && errors[form.name] && (
                      <Box mt={1}>
                        <FormHelperText error>
                          {errors[form.name]}
                        </FormHelperText>
                      </Box>
                    )}
                  </div>
                ) : ['select'].indexOf(type) !== -1 ? (
                  <div style={{ display: !form.show ? 'block' : 'none' }}>
                    <Box>
                      <Typography variant="h6">
                        {form.label}
                        {form.required && (
                          <Box component={'span'} color={'primary.main'}>
                            *
                          </Box>
                        )}
                      </Typography>{' '}
                    </Box>
                    <FormControl variant="outlined" fullWidth>
                      <Select
                        labelId="demo-simple-select-outlined-label"
                        id="demo-simple-select-outlined"
                        onBlur={handleBlur}
                        onChange={handleChange}
                        name={form.name}
                        value={values[form.name]}
                      >
                        {form.selectOptions.map(item => (
                          <MenuItem value={item.value}>{item.label}</MenuItem>
                        ))}
                      </Select>
                    </FormControl>
                    {touched[form.name] && errors[form.name] && (
                      <Box mt={1}>
                        <FormHelperText error>
                          {errors[form.name]}
                        </FormHelperText>
                      </Box>
                    )}
                  </div>
                ) : ['language'].indexOf(type) !== -1 ? (
                  <div style={{ display: !form.show ? 'block' : 'none' }}>
                    <Languages
                      name={form.name}
                      setFieldValue={setFieldValue}
                      values={values[form.name]}
                    />
                    {touched[form.name] && errors[form.name] && (
                      <Box mt={1}>
                        <FormHelperText error>
                          {errors[form.name]}
                        </FormHelperText>
                      </Box>
                    )}
                  </div>
                ): ['file_drop_zone'].indexOf(type) !== -1 ? (
                   <Box>
                      <FilesDropzone
                        name={form.name}
                        field={form}
                        defaultImgSrc={form.imgSrc && form.imgSrc}
                        onChange={(content) =>
                          setFieldValue(form.name, content)
                        }
                      />
                      {(errors )[form.name] && (
                        <Box mt={1}>
                          <FormHelperText error>
                            {(errors)[form.name]}
                          </FormHelperText>
                        </Box>
                      )}
                     
                    </Box>
                
                ) : (
                  <div />
                )}
              </Grid>
            ))}
          </Grid>
          {renderSubmit &&
            renderSubmit(isSubmitting, errors, touched,setFieldTouched, handleSubmit)}
        </form>
      )}
    </Formik>
  );
}
