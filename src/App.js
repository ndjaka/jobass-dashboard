import 'react-perfect-scrollbar/dist/css/styles.css';
import React from 'react';
import { useRoutes } from 'react-router-dom';
import { Provider as StoreProvider } from 'react-redux';
import { ThemeProvider } from '@material-ui/core';

import './mixins/chartjs';

import routes from './routes';
import { SnackbarProvider } from 'notistack';

import { configureStore } from './store';
import GlobalStyles from './components/GlobalStyles';
import theme from './theme';

const store = configureStore();
const App = () => {
  const routing = useRoutes(routes);

  return (
    <StoreProvider store={store}>
      <ThemeProvider theme={theme}>
        <GlobalStyles />
        <SnackbarProvider
          maxSnack={5}
          anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
        >
          {routing}
        </SnackbarProvider>
      </ThemeProvider>
    </StoreProvider>
  );
};

export default App;
