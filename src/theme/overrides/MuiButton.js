export default {
  root: {},
  contained: {
    borderRadius: 25,
    height: 48,
    boxShadow:
      "0 1px 1px 0 rgba(0,0,0,0.14), 0 2px 1px -1px rgba(0,0,0,0.12), 0 1px 3px 0 rgba(0,0,0,0.20)",
    backgroundColor: "#FFFFFF"
  },
  containedSecondary: {
    backgroundColor: "#56CCF2",
    color: "#fff",
    "&:hover": {
      backgroundColor: "#3f91af"
    }
  }
};
