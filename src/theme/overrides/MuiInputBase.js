
export default {
  root: {
    backgroundColor: '#fff'
  },
  input: {
    padding:'12.5px 16px',
    '&::placeholder': {
      opacity: 1,
     
    }
  }
};
